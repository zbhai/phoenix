import os
import subprocess
import json
from pathlib import Path

# enables counting list items
from operator import length_hint
# .env parser
from dotenv import load_dotenv
# getpass enables secure password input
from getpass import getpass
# download files containing the domain specific information
from urllib.request import urlparse, urlretrieve
# pandas handles table data
import pandas as pd
# The nest_asyncio module enables the nesting of asynchronous functions within an already running async loop.
import nest_asyncio
nest_asyncio.apply()

# colored print
from colorist import Color, BrightColor, bright_yellow, magenta, red, green
# phoenix is the framework & webservice from arize (https://docs.arize.com/phoenix)
import phoenix as px
from phoenix.evals import OpenAIModel, LiteLLMModel, llm_generate, HallucinationEvaluator, QAEvaluator, run_evals
from phoenix.session.evaluation import get_retrieved_documents, get_qa_with_reference
from phoenix.evals import RelevanceEvaluator, run_evals
from phoenix.trace import DocumentEvaluations, SpanEvaluations

# llama index boilerplates chunking, verctorizing, storing querying aso. of private data
from llama_index.core import set_global_handler, SimpleDirectoryReader, VectorStoreIndex
from llama_index.core.node_parser import SimpleNodeParser
from llama_index.llms.openai import OpenAI

import numpy as np
from sklearn.metrics import ndcg_score

##################################################
### PREPARING & STARTING
##################################################

##########
# SET VARS
##########
# Load environment variables from .env file
if not os.path.isfile('./.env'):
  raise RuntimeError("Aborting: No .env file found.")
load_dotenv()

# tell llama_index to send all infos to the phoenix instance
set_global_handler("arize_phoenix")

# ##########
# # LOAD OPENAPI KEY
# ##########
if not (openai_api_key := os.getenv("OPENAI_API_KEY")):
    openai_api_key = getpass("🔑 Enter your OpenAI API key: ")
os.environ["OPENAI_API_KEY"] = openai_api_key
# Define the LLM
llm = OpenAI(model="gpt-4")

##########
# START PHOENIX
##########
# check for running process
process = subprocess.run(["lsof", f"-iTCP:{os.environ['PHOENIX_PORT']}"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
output = process.stdout.decode("utf8")
if len(output.strip()) > 1:
  print(f"Aborting: Error while attempting to bind on address ('0.0.0.0', {os.environ['PHOENIX_PORT']}): address already in use")
  os._exit(1)

# launch phoenix
green(f"Launching phoenix application")
session = px.launch_app()
("phoenix URL", px.active_session().url)

##################################################
### PREPARING FILES FOR RAG
##################################################

##########
# LOAD DOMAIN SPECIFIC INFORMATION FILES
##########
# create array of files to download
urls = os.environ['DOMAIN_DOCS'].split(',')
# create download folder
Path(os.environ['DOMAIN_DOWNLOAD_FOLDER']).mkdir(parents=True, exist_ok=True)
# for each file to download
for url in urls:
  # parse url
  source = urlparse(os.environ['DOMAIN_BASEPATH'] + url)
  # transfer path into filename (because filenames may occur multiple times in different paths)
  targetFilename = source.path.replace('/',':')
  # remember full target path
  targetPath = os.environ['DOMAIN_DOWNLOAD_FOLDER']+'/'+ targetFilename
  # output
  print(f'downloading {source.path} \t>\t {targetPath}')
  # download
  urlretrieve(os.environ['DOMAIN_BASEPATH'] + url, targetPath)
  
# load docs into llamaIndex
documents = SimpleDirectoryReader(input_dir=os.environ['DOMAIN_DOWNLOAD_FOLDER']).load_data()


##########
# CHUNK & VECTORIZE FILES
##########
green(f"Building index with a chunk_size of {os.environ['DOCUMENTS_CHUNK_SIZE']} for {length_hint(documents)} docs")
# define parsing options incl. regex, paragraph_separator aso.
node_parser = SimpleNodeParser.from_defaults(chunk_size=int(os.environ['DOCUMENTS_CHUNK_SIZE']))
# generate nodes based on the parser
nodes = node_parser.get_nodes_from_documents(documents)
# Vectorize nodes
# very first time OPENAI_API_KEY is needed in this script
vector_index = VectorStoreIndex(nodes)
print(f'created {length_hint(nodes)} chunks')
countQuestions = length_hint(nodes) * int(os.environ['QUESTIONS_PER_CHUNK'])
# Build a QueryEngine and start querying.
query_engine = vector_index.as_query_engine()

##########
# CREATE THE QUESTION-CONTEXT PAIRS
##########
green(f'Generating {countQuestions} questions. {os.environ['QUESTIONS_PER_CHUNK']} per chunk')

# create a dataframe of all the document chunks that were indexed
document_chunks_df = pd.DataFrame({"text": [node.get_text() for node in nodes]})

# template to generate {QUESTIONS_PER_CHUNK} questions per chunk
generate_questions_template = f"""\
Context information is below.

---------------------
{{text}}
---------------------

Given the context information and not prior knowledge.
generate only questions based on the below query.

You are a Teacher/ Professor. Your task is to setup \
{os.environ['QUESTIONS_PER_CHUNK']} questions for an upcoming \
quiz/examination. The questions should be diverse in nature \
across the document. Restrict the questions to the \
context information provided."

Output the questions in JSON format with the keys question_1, question_2, question_3.
"""

# define parser to fetch questions from the response
def output_parser(response: str, index: int):
  try:
    return json.loads(response)
  except json.JSONDecodeError as e:
    return {"__error__": str(e)}
  
# prompt template to LLM and store > questions_df 
# You can choose among multiple models supported by LiteLLM (https://docs.litellm.ai/docs/providers)
questions_df = llm_generate(
  dataframe=document_chunks_df,
  template=generate_questions_template,
  model=LiteLLMModel(
    model="ollama/llama2",
  ),
  output_parser=output_parser,
  concurrency=20,
)

# Construct a dataframe of the questions and the document chunks
questions_with_document_chunk_df = pd.concat([questions_df, document_chunks_df], axis=1)
questions_with_document_chunk_df = questions_with_document_chunk_df.melt(
  id_vars=["text"], value_name="question"
).drop("variable", axis=1)
# If the above step was interrupted, there might be questions missing. Clean up the dataframe.
questions_with_document_chunk_df = questions_with_document_chunk_df[
  questions_with_document_chunk_df["question"].notnull()
]
# debug print 
# magenta(questions_with_document_chunk_df.head(10))

##################################################
### RETRIEVAL EVALUATION 
##################################################
green('Starting retrieval evaluation')
##########
# GENERATE THE ANSWERS
######## 
print(f'Generating the answers for each question')
# loop over the questions and generate the answers
for _, row in questions_with_document_chunk_df.iterrows():
  question = row["question"]
  response_vector = query_engine.query(question)
  # debug print question-answer pair
  # print(f"Question: {Color.MAGENTA}{question}{Color.OFF}\nAnswer: {BrightColor.MAGENTA}{response_vector.response}{Color.OFF}\n")

# extract all the retrieved documents from the traces logged to phoenix
print('extracting the retrieved documents from phoenix traces')
retrieved_documents_df = get_retrieved_documents(px.Client())

##########
# CALCULATE RELEVANCE
######## 
# use Phoenix's LLM Evals to evaluate the relevance of the retrieved documents with regards to the query. 
# Note the turned on explanations which prompts the LLM to explain it's reasoning. 
# This can be useful for debugging and for figuring out potential corrective actions.
print('calculating relevance of the documents to the query')
relevance_evaluator = RelevanceEvaluator(OpenAIModel(model="gpt-4-turbo-preview"))
retrieved_documents_relevance_df = run_evals(
  evaluators=[relevance_evaluator],
  dataframe=retrieved_documents_df,
  provide_explanation=True,
  concurrency=20,
)[0]

# combine the documents with the relevance evaluations to compute retrieval metrics. 
# These metrics will help to understand how well the RAG system is performing.
documents_with_relevance_df = pd.concat(
  [retrieved_documents_df, retrieved_documents_relevance_df.add_prefix("eval_")], axis=1
)
##########
# NCDG@2
##########
print(f'Computing NCDG@2')
# function
def _compute_ndcg(df: pd.DataFrame, k: int):
  """Compute NDCG@k in the presence of missing values"""
  n = max(2, len(df))
  eval_scores = np.zeros(n)
  doc_scores = np.zeros(n)
  eval_scores[: len(df)] = df.eval_score
  doc_scores[: len(df)] = df.document_score
  try:
    return ndcg_score([eval_scores], [doc_scores], k=k)
  except ValueError:
    return np.nan
# run
ndcg_at_2 = pd.DataFrame(
  {"score": documents_with_relevance_df.groupby("context.span_id").apply(_compute_ndcg, k=2)}
)
##########
# PRECISION@2
##########
print(f'Computing Precision@2')
precision_at_2 = pd.DataFrame(
  {
    "score": documents_with_relevance_df.groupby("context.span_id").apply(
        lambda x: x.eval_score[:2].sum(skipna=False) / 2
    )
  }
)
##########
# HIT
##########
print(f'Computing HIT')
hit = pd.DataFrame(
  {
    "hit": documents_with_relevance_df.groupby("context.span_id").apply(
      lambda x: x.eval_score[:2].sum(skipna=False) > 0
    )
  }
)
##########
# COMBINE METRICS INTO ONE DATAFRAME
##########
print(f'Creating dataframe of all metrics')
retrievals_df = px.Client().get_spans_dataframe("span_kind == 'RETRIEVER'")
rag_evaluation_dataframe = pd.concat(
  [
    retrievals_df["attributes.input.value"],
    ndcg_at_2.add_prefix("ncdg@2_"),
    precision_at_2.add_prefix("precision@2_"),
    hit,
  ],
  axis=1,
)
# debug
# magenta(rag_evaluation_dataframe)

##########
# AGGREGATE THE SCORES ACROSS THE RETRIEVALS
##########
print(f'Aggregated metrics are:')
results = rag_evaluation_dataframe.mean(numeric_only=True)
magenta(results)

# SEND PERFORMANCE INFO TO PHOENIX
px.Client().log_evaluations(
  SpanEvaluations(dataframe=ndcg_at_2, eval_name="ndcg@2"),
  SpanEvaluations(dataframe=precision_at_2, eval_name="precision@2"),
  DocumentEvaluations(dataframe=retrieved_documents_relevance_df, eval_name="relevance"),
)
##################################################
### RESPONSE EVALUATION 
##################################################
green(f'Starting response evaluation')

# fetching question, context, and response (input, reference, and output) into one dataframe
qa_with_reference_df = get_qa_with_reference(px.Client())
# debug print 
# magenta(qa_with_reference_df)

##########
# CALCULATE CORRECTNESS & HALLUCINATIONS
##########
# measure how well the LLM is responding to the queries
# details: https://docs.arize.com/phoenix/evaluation/how-to-evals/running-pre-tested-evals/q-and-a-on-retrieved-data
qa_evaluator = QAEvaluator(OpenAIModel(model="gpt-4-turbo-preview"))
hallucination_evaluator = HallucinationEvaluator(OpenAIModel(model="gpt-4-turbo-preview"))

print('calculate performance of QA correctness & hallucinations')
qa_correctness_eval_df, hallucination_eval_df = run_evals(
    evaluators=[qa_evaluator, hallucination_evaluator],
    dataframe=qa_with_reference_df,
    provide_explanation=True,
    concurrency=20,
)
# debug
# magenta(qa_correctness_eval_df.head())
# magenta(hallucination_eval_df.head())

# aggregate results to get a sense of how well the LLM is answering the questions given the context
qa_correctness_eval_df.mean(numeric_only=True)
hallucination_eval_df.mean(numeric_only=True)

# send QA performance and Hallucinations performance to Phoenix for visualization
print('send aggregated evaluations to phoenix')
px.Client().log_evaluations(
    SpanEvaluations(dataframe=qa_correctness_eval_df, eval_name="Q&A Correctness"),
    SpanEvaluations(dataframe=hallucination_eval_df, eval_name="Hallucination"),
)

##################################################
### CLEAN END 
##################################################
green('finished evaluation process')
print("See result here: ", px.active_session().url)
bright_yellow("Press Enter to exit...")
input()
px.close_app()

##################################################
### SHITTY SNIPPETS, NEVER SUPPOSED TO RUN
##################################################
# if process.returncode != 0:
#   print(process.stderr.decode("utf8"))
#   raise RuntimeError(f"Returncode {process.returncode} Und das war nicht gut und der error ist ")
# print(process.returncode)
# print(process.stdout.decode("utf8"))
# output = process.stdout.decode("utf8")
# if len(output.strip()) > 1:
#   print("ham wir")
# else:
#   print("ham wir nicht")
# print(process.stderr.decode("utf8"))

