# Purposes
This repository should enable you to compare LLM performances on RAG systems. 

Imagine a Professor - Studend situation:

1. The script fetches RAG files and chunks them. 
1. The prof (LLM1) creates a bunch of questions per chunk.
1. The studend (LLM2) answers these questions.
1. The professor then evaluates the questions in continuous and comparable metrics such as hallucination and correctness.

The result is detailed information on the quality of each answer and an overview of the metrics of the LLM as a whole (i.e. ndcg@2 & precision@2).

# Prerequisits
- python3 installed
- [openAI API Key](https://auth.openai.com/)

# Install
```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
cp ./.env.template ./.env
```

# Configure
populate the `.env` file with proper information

# Start
```
python3 evaluateRAG.py
```
acccess webservice via browser, i.e.
`firefox http:<HOSTNAME>:<PORT>`

# Sources
- [YT: RAG Time! Evaluate RAG with LLM Evals and Benchmarking](https://www.youtube.com/watch?v=LrMguHcbpO8)
- [Phoenix Docs](https://docs.arize.com/phoenix)


# Planned
- [ ] ability to choose student LLM
- [ ] option to use local LLM via ollama